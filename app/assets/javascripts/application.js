// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(function() {
  $(".date").datepicker();
  $(".date").datepicker("setDate", new Date());

  $("#check-availability").click(checkAvailability);

  function checkAvailability() {
    var start = $("#start-date").val() + " " + $("#start-time").val();
    var end = $("#end-date").val() + " " + $("#end-time").val();

    /* Replace this with real logic, or just submit the form */
    if (Math.floor((Math.random() * 2))) {
      parkingNotAvailable(start, end);
    } else {
      parkingAvailable(start, end, Math.floor((Math.random() * 25) + 1));
    }
  }

  function parkingNotAvailable(start, end) {
    $(".quote-start").html(start);
    $(".quote-end").html(end);
    $("#parking-available").hide();
    $("#parking-not-available").show();
  }
  function parkingAvailable(start, end, amount) {
    $(".quote-start").html(start);
    $(".quote-end").html(end);
    $(".quote-amount").html(amount);
    $("#parking-available").show();
    $("#parking-not-available").hide();
  }
});