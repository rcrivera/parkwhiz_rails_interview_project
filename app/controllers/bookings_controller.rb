class BookingsController < ApplicationController

  def create
  	booking = Booking.create(booking_params)
  	respond_to do |format|
    	format.js
    end
  end

  private

  def booking_params
		params.require(:booking).permit(:customer_name, :plate_number, :time_start, :time_end, :listing_id)
  end
end
