class SiteController < ApplicationController

  def index
  	@cities = Listing.select(:city).distinct
  end

  def check_availability
  	@start_date = create_datetime(params['start-date'], params['start-time'])
  	@end_date = create_datetime(params['end-date'], params['end-time'])
    @hours = (@end_date - @start_date) * 24.hours/ 3600.seconds
  	city = params['city']

  	bookings = Listing.joins('left outer join bookings on listings.id = bookings.listing_id').where("listings.city = ? AND ? < bookings.time_end AND ? > bookings.time_start", city, @start_date, @end_date).group('bookings.listing_id').count

  	not_available = []

  	bookings.each do |location, occupancy|
  		listing = Listing.find(location)
  		not_available << listing.id if occupancy >= listing.available
  	end
		
		@available_locations = Listing.where(:city => city).where.not(id: not_available).order(:price_per_hour)

  	respond_to do |format|
  		if @available_locations.empty?
  			format.js { render :template => "site/unavailable" }
  		else
  			format.js { render :template => "site/available" }
  		end
    end
  end

  private

  def create_datetime(date, time)
  	DateTime.strptime(date+'-'+time, '%m/%d/%Y-%H:%M')
  end

end